const Boom = require('boom');
const Joi = require('joi');

const internals = {
    optionsSchema: Joi.object({
        validVersions: Joi.array().items(Joi.number().integer()).min(1).required(),
        select: Joi.array().items(Joi.string()),
    }),
};

exports.register = function register(server, options, next) {
    const validateOptions = internals.optionsSchema.validate(options);

    if (validateOptions.error) {
        throw validateOptions.error;
    }

    const validVersions = validateOptions.value.validVersions.map((version) => `v${version}`);

    server.ext('onRequest', (request, reply) => {
        const path = request.url.path;
        if (path.startsWith('/v') && !validVersions.some((api) => path.startsWith(`/${api}/`))) {
            const errorMessage = `Invalid API version! Valid API versions: ${validVersions.join()}`;
            return reply(Boom.badRequest(errorMessage));
        }
        return reply.continue();
    });

    server.route([
        {
            method: 'GET',
            path: '/SupportedVersions',
            config: {
                tags: ['api'],
                description: 'List supported API versions',
            },
            handler: (request, reply) => reply(validVersions),
        },
    ]);

    return next();
};

const projectPkg = require('../../package.json');
const pkg = {
    name: 'api-version',
    version: projectPkg.version,
};

exports.register.attributes = {
    pkg,
};
