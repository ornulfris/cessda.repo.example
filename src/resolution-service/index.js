'use strict';

const internals = {};

internals.resolve = (mappingFile, repositoryUrl) => {
    require.cache[mappingFile] = null;
    const mappings = require(mappingFile);
    return mappings[repositoryUrl];
};

exports.register = function register(plugin, options, next) {
    plugin.route({
        method: 'GET',
        path: '/v0/ResolveRepositoryURL',
        handler: (request, reply) => {
            const repoHandlerURI = options.mapping[request.query.Repository] ||
                internals.resolve(options.mappingFile, request.query.Repository);
            if (repoHandlerURI === undefined) {
                reply({ message: 'Repository URL not found.' }).code(404);
            } else {
                reply({ repositoryHandlerUrl: repoHandlerURI });
            }
        },
    });

    next();
};

const projectPkg = require('../../package.json');
const pkg = {
    name: 'resolution-service',
    version: projectPkg.version,
};

exports.register.attributes = {
    pkg,
};
