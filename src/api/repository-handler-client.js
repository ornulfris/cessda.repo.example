'use strict';

const Wreck = require('wreck');
const Boom = require('boom');

const wreckJSON = Wreck.defaults({
    headers: {
        'Content-Type': 'application/json',
    },
});

/**
 * Takes object with client headers and filters it
 * obtaining headers that should be forwarded, namely:
 * X-Request-ID, 'If-Modified-Since', 'If-None-Match'.
 *
 * @param headers object of header keys and values
 * @returns headers object of headers to forward
 */
const forwardHeaders = (headers) => {
    const headerList = ['X-Request-ID', 'If-Modified-Since', 'If-None-Match'];
    const headersToForward = {};
    headerList.forEach((header) => {
        const headerValue = headers[header] || headers[header.toLowerCase()];
        if (header) headersToForward[header] = headerValue;
    });
    return headersToForward;
};

const getResource = (repositoryHandler, url, headers, log) => {
    const responsePromise = new Promise((resolve, reject) => {
        const requestHeaders = forwardHeaders(headers);
        wreckJSON.get(url, { json: true, headers: requestHeaders }, (err, res, payload) => {
            if (err) {
                const error =
                    Boom.badImplementation('Could not get resource from repository handler.');
                log({ level: 'error', message: 'remote-request', url, detail: err });
                reject(error);
            } else if (res.statusCode === 200) { // Ok reponse
                resolve({ statusCode: 200, headers: res.headers, payload, url });
            } else if (res.statusCode === 304) { // Up to date
                log({ message: 'remote-response', url, payload: {}, statusCode: res.statusCode });
                resolve({ statusCode: 304, headers: res.headers, payload: {} });
            } else if (res.statusCode === 400) { // Bad request
                const error = Boom.badRequest(payload.message);
                log({ level: 'warning', message: 'remote-response', url, payload, statusCode: res.statusCode });
                reject(error);
            } else if (res.statusCode === 404) { // Resource not found
                const error = Boom.notFound(payload.message);
                log({ level: 'warning', message: 'remote-response', url, payload, statusCode: res.statusCode });
                reject(error);
            } else if (res.statusCode === 500) {
                const error = Boom.badImplementation(payload.message);
                log({ level: 'error', message: 'remote-response', url, payload, statusCode: res.statusCode });
                reject(error);
            } else {
                const error = Boom.badImplementation(`Unknown status code: ${res.statusCode}`);
                log({ level: 'error', message: 'unknown-status-code', url, payload, statusCode: res.statusCode });
                reject(error);
            }
        });
    });

    return responsePromise;
};

const getRecordV0 = (repositoryHandler, repositoryUrl, headers, identifier, recordType, log) => {
    const requestUrl = `${repositoryHandler}/v0/GetRecord/${recordType}/${identifier}?Repository=${repositoryUrl}`;
    return getResource(repositoryHandler, requestUrl, headers, log);
};

const ensureLastModifiedHeader = (log) =>
    (res) => new Promise((resolve, reject) => {
        const lastModified = res.headers['Last-Modified'] || res.headers['last-modified'];
        const url = res.url;
        const payload = res.payload;
        if (lastModified === undefined || lastModified === null) {
            log({ level: 'error', message: 'missing-last-modified-header', url, payload, statusCode: res.statusCode });
            reject(Boom.badImplementation('Missing Last-Modified HTTP header'));
        } else if (isNaN(new Date(lastModified).getTime())) { // https://github.com/moment/moment/issues/116
            log({
                level: 'error',
                message: 'incorrect-last-modified-header',
                url,
                payload,
                statusCode: res.statusCode,
            });
            reject(Boom.badImplementation('Incorrect format for Last-Modified HTTP header'));
        } else {
            resolve(res);
        }
    });

exports.getQuestionV0 = (repositoryHandler, repositoryUrl, headers, questionIdentifier, log) =>
    getRecordV0(repositoryHandler, repositoryUrl, headers, questionIdentifier, 'Question', log)
        .then(ensureLastModifiedHeader(log));

exports.getStudyV0 = (repositoryHandler, repositoryUrl, headers, studyIdentifier, log) =>
    getRecordV0(repositoryHandler, repositoryUrl, headers, studyIdentifier, 'Study', log)
        .then(ensureLastModifiedHeader(log));

exports.getStudyGroupV0 = (repositoryHandler, repositoryUrl, headers, studyGroupIdentifier, log) =>
    getRecordV0(repositoryHandler, repositoryUrl, headers, studyGroupIdentifier, 'StudyGroup', log)
        .then(ensureLastModifiedHeader(log));

exports.getVariableV0 = (repositoryHandler, repositoryUrl, headers, variableIdentifier, log) =>
    getRecordV0(repositoryHandler, repositoryUrl, headers, variableIdentifier, 'Variable', log)
        .then(ensureLastModifiedHeader(log));

exports.listRecordHeadersv0 = (repositoryHandler, repositoryUrl, headers, log, type) => {
    let requestUrl = `${repositoryHandler}/v0/ListRecordHeaders`;
    if (type !== undefined && type !== null) requestUrl = `${requestUrl}/${type}`;
    requestUrl = `${requestUrl}?Repository=${encodeURIComponent(repositoryUrl)}`;

    return getResource(repositoryHandler, requestUrl, headers, log);
};

exports.listSupportedRecordTypesv0 = (repositoryHandler, repositoryUrl, headers, log) => {
    let requestUrl = `${repositoryHandler}/v0/ListSupportedRecordTypes`;
    requestUrl = `${requestUrl}?Repository=${encodeURIComponent(repositoryUrl)}`;
    return getResource(repositoryHandler, requestUrl, headers, log);
};
