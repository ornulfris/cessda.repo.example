const Joi = require('joi');

const languageString = Joi.object({
    en: Joi.string(), // English not required, but recommended.
}).pattern(/^[a-z]{2}$/, Joi.string());

const downloadURL = Joi.object({
    fileFormat: Joi.string().required().example('csv'),
    URL: Joi.string().uri().required().example('http://example.com/Download/...'),
});

exports.studySchema = Joi.object({
    identifier: Joi.string().required(),
    recordType: Joi.string().valid('Study').required(),
    lastModified: Joi.date().required().example('1970-01-01T01:00:00Z'),
    persistentIdentifier: Joi.string().allow(null).example('doi:10.3886/ICPSR09212.v2'),
    downloadURL: Joi.array().items(downloadURL),
    orderURL: Joi.string().uri().allow(null).example('http://originating-repository.com/Order/Study/S123'),
    landingPageURL: Joi.string().uri().allow(null).example('http://originating-repository.com/Study/S123'),
    prefLabel: languageString,
    title: languageString.required(),
    subtitle: languageString,
    alternative: languageString,
    abstract: languageString,
    analysisUnit: languageString,
    ddiFile: Joi.array().items(Joi.string()),
    fundedBy: Joi.array().items(languageString),
    inGroup: Joi.array().items(Joi.string()),
    instrument: Joi.array().items(Joi.string()),
    kindOfData: languageString,
    subject: Joi.array().items(languageString),
    product: Joi.string().allow(null), // ID of logical dataset file
    purpose: languageString,
    universe: languageString,
    variable: Joi.array().items(Joi.string()),
    available: Joi.date().allow(null).example('1970-01-01T01:00:00Z'),
    temporal: Joi.object({
        startDate: Joi.date().allow(null).example('1970-01-01T01:00:00Z'),
        endDate: Joi.date().allow(null).example('1971-01-01T01:00:00Z'),
    }),
    spatial: Joi.array().items(languageString),
});


const codeList = Joi.array().items(Joi.object({
    prefLabel: languageString.required(),
    notation: Joi.string().required(),
}));

exports.variableSchema = Joi.object({
    identifier: Joi.string().required(),
    recordType: Joi.string().valid('Variable').required(),
    lastModified: Joi.date().required().example('1970-01-01T01:00:00Z'),
    persistentIdentifier: Joi.string().allow(null).example('doi:10.3886/ICPSR09212.v2'),
    prefLabel: languageString,
    description: languageString,
    subject: Joi.array().items(languageString),
    notation: Joi.string().allow(null),
    basedOn: Joi.string().allow(null),
    analysisUnit: languageString,
    concept: languageString,
    question: Joi.array().items(Joi.string()),
    universe: languageString,
    inStudy: Joi.array().items(Joi.string()),
    representation: Joi.string().allow(null),
    codeList,
});

exports.studyGroupSchema = Joi.object({
    identifier: Joi.string().required(),
    recordType: Joi.string().valid('StudyGroup').required(),
    lastModified: Joi.date().required().example('1970-01-01T01:00:00Z'),
    persistentIdentifier: Joi.string().allow(null).example('doi:10.3886/ICPSR09212.v2'),
    prefLabel: languageString,
    title: languageString.required(),
    subtitle: languageString,
    abstract: languageString,
    alternative: languageString,
    available: Joi.date().allow(null).example('1970-01-01T01:00:00Z'),
    purpose: languageString,
    fundedBy: Joi.array().items(languageString),
    inGroup: Joi.array().items(Joi.string()),
    kindOfData: languageString,
    universe: languageString,
    ddiFile: Joi.array().items(Joi.string()),
});

exports.questionSchema = Joi.object({
    identifier: Joi.string().required(),
    recordType: Joi.string().valid('Question').required(),
    lastModified: Joi.date().required().example('1970-01-01T01:00:00Z'),
    persistentIdentifier: Joi.string().allow(null).example('doi:10.3886/ICPSR09212.v2'),
    questionText: languageString.required(),
    inVariable: Joi.array().items(Joi.string()),
    representation: Joi.string().allow(null),
    codeList,
});

const recordType = Joi.string().valid('StudyGroup', 'Study', 'Variable', 'Question');

exports.recordTypeSchema = recordType;

const recordHeader = Joi.object({
    identifier: Joi.string().required(),
    recordType: Joi.string().valid('RecordHeader').required(),
    type: recordType.required(),
    lastModified: Joi.date().required().example('1971-01-01T01:00:00Z'),
});

exports.recordHeadersListSchema = Joi.array().items(recordHeader).required();
exports.supportedRecordTypesListSchema = Joi.array().items(recordType).required();
