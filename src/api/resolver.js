'use strict';

const Boom = require('boom');
const Wreck = require('wreck');

exports.resolveRepositoryUrl = (resolutionURL, repositoryURL, headers) => {
    const resolvePromise = new Promise((resolve, reject) => {
        const path = '/v0/ResolveRepositoryURL';
        const url = `${resolutionURL}${path}?Repository=${repositoryURL}`;
        Wreck.get(url, { json: true, headers: { 'X-Request-ID': headers['x-request-id'] } }, (err, res, payload) => {
            if (err) {
                const error = Boom.badImplementation(
                    'Could not get repository handler url from resolution service.'
                );
                reject(error);
            } else if (res.statusCode === 200) { // Ok!
                resolve(payload.repositoryHandlerUrl);
            } else if (res.statusCode === 404) { // Resource not found
                const error = Boom.notFound(payload.message);
                reject(error);
            } else if (res.statusCode === 500) {
                const error = Boom.badImplementation(payload.message);
                reject(error);
            }
        });
    });
    return resolvePromise;
};
