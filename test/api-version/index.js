const Code = require('code');
const Lab = require('lab');
const cessdaMetadataHarvester = require('../../index');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;

cessdaMetadataHarvester((err, server) => {
    const apiServer = server.select('api');

    describe('api version validation', () => {
        it('unknown api version gives 400 bad request', (done) => {
            apiServer.inject('/v99999/error', (res) => {
                expect(res.statusCode).to.equal(400);
                done();
            });
        });

        it('supported api version but unknown endpoint gives 404 not found', (done) => {
            apiServer.inject('/v0/missing', (res) => {
                expect(res.statusCode).to.equal(404);
                done();
            });
        });

        it('list supported API versions', (done) => {
            apiServer.inject('/SupportedVersions', (res) => {
                expect(res.statusCode).to.equal(200);
                expect(res.result).to.deep.equal(['v0']);
                done();
            });
        });
    });
});
