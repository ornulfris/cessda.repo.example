const Code = require('code');
const Lab = require('lab');
const nock = require('nock');
const cessdaMetadataHarvester = require('../../index');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;
const afterEach = lab.afterEach;

const repositoryHandler = nock('http://repositoryhandler.example.com');
const resolutionService = nock('http://localhost:8081');

cessdaMetadataHarvester((err, server) => {
    afterEach((done) => {
        nock.cleanAll();
        done();
    });
    const apiServer = server.select('api');

    describe('x-request-id plugin', () => {
        describe('onPreRequest Handler', () => {
            it('should add an x-request-id header if not present', (done) => {
                apiServer.inject('/', (res) => {
                    expect(res.request.headers['x-request-id']).to.exist();
                    done();
                });
            });

            it('should not add an x-request-id header if already present', (done) => {
                const uuid = '110ec58a-a0f2-4ac4-8393-c866d813b8d1';
                apiServer.inject({ url: '/', headers: { 'x-request-id': uuid } }, (res) => {
                    expect(res.request.headers['x-request-id']).to.equal(uuid);
                    done();
                });
            });
        });

        describe('onPreResponse Handler', () => {
            it('should respond with an x-request-id header for a 200 request', (done) => {
                apiServer.inject('/SupportedVersions', (res) => {
                    expect(res.headers['x-request-id']).to.exist();
                    expect(res.statusCode).to.equal(200);
                    done();
                });
            });

            it('should respond with an x-request-id header for a 500 request', (done) => {
                repositoryHandler
                    .get('/v0/ListRecordHeaders')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(500, { message: 'Internal Handler Server Error' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/ListRecordHeaders?Repository=http://repository.example.com', (res) => {
                    expect(res.headers['x-request-id']).to.exist();
                    expect(res.statusCode).to.equal(500);
                    done();
                });
            });

            it('should respond with an x-request-id header for a 404 request', (done) => {
                apiServer.inject('/xyz', (res) => {
                    expect(res.headers['x-request-id']).to.exist();
                    expect(res.statusCode).to.equal(404);
                    done();
                });
            });
        });
    });
});
