const Code = require('code');
const Lab = require('lab');
const nock = require('nock');
const Hoek = require('hoek');
const client = require('../../src/api/repository-handler-client');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const afterEach = lab.afterEach;
const expect = Code.expect;
const repositoryHandlerUrl = 'http://repositoryhandler.example.com';

afterEach((done) => {
    nock.cleanAll();
    done();
});

const emptyLogger = Hoek.ignore;

describe('repository-handler-client', () => {
    describe('list record headers v0', () => {
        it('returns list of valid record headers for valid repository', (done) => {
            const records = [
                {
                    type: 'Study',
                    recordType: 'RecordHeader',
                    identifier: 'MD2014',
                    lastModified: '2006-01-01T01:00:00.000Z',
                },
                {
                    type: 'Variable',
                    recordType: 'RecordHeader',
                    identifier: 'MD2014_V1',
                    lastModified: '2006-01-01T01:00:00.000Z',
                },
            ];

            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, records);

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {}, emptyLogger)
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(result.payload).to.deep.equal(records);
                    done();
                })
                .catch(done);
        });

        it('returns list of valid study record headers for study type', (done) => {
            const records = [
                {
                    identifier: 'MD2014',
                    type: 'Study',
                    lastModified: '2006-01-01T01:00:00.000Z',
                },
                {
                    identifier: 'MD2014_V1',
                    type: 'Study',
                    lastModified: '2006-01-01T01:00:00.000Z',
                },
            ];

            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders/Study')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, records);

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                emptyLogger,
                'Study')
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(result.payload).to.deep.equal(records);
                    done();
                })
                .catch(done);
        });

        it('returns 304 unmodified when repository handler returns 304', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders')
                .query({ Repository: 'http://repository.example.com' })
                .reply(304);

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                emptyLogger)
                .then((result) => {
                    expect(result.statusCode).to.equal(304);
                    done();
                })
                .catch(done);
        });

        it('returns 400 error when repository handler returns 400', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders')
                .query({ Repository: 'http://repository.example.com' })
                .reply(400, { message: 'Bad request, missing info' });

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                emptyLogger
            )
                .then((result) => done(new Error(result)))
                .catch((error) => {
                    expect(error.isBoom).to.be.true();
                    expect(error.output.statusCode).to.equal(400);
                    expect(error.output.payload.error).to.equal('Bad Request');
                    expect(error.output.payload.message).to.equal('Bad request, missing info');
                    done();
                })
                .catch(done);
        });

        it('returns 500 error when repository handler returns unknown status code', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders')
                .query({ Repository: 'http://repository.example.com' })
                .reply(410, { message: 'Resource missing' });

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                emptyLogger
            )
                .then((result) => new Error(result))
                .catch((error) => {
                    expect(error.isBoom).to.be.true();
                    expect(error.output.statusCode).to.equal(500);
                    expect(error.output.payload.error).to.equal('Internal Server Error');
                    expect(error.output.payload.message).to.equal('An internal server error occurred');
                    done();
                })
                .catch(done);
        });

        it('returns 404 error when repository handler returns 404', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders')
                .query({ Repository: 'http://repository.example.com' })
                .reply(404, { message: 'Repository Not Found' });

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                emptyLogger
            )
                .then((result) => done(new Error(result)))
                .catch((error) => {
                    expect(error.isBoom).to.be.true();
                    expect(error.output.statusCode).to.equal(404);
                    expect(error.output.payload.error).to.equal('Not Found');
                    expect(error.output.payload.message).to.equal('Repository Not Found');
                    done();
                })
                .catch(done);
        });

        it('returns 500 error when repository handler returns 500', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/ListRecordHeaders')
                .query({ Repository: 'http://repository.example.com' })
                .reply(500, { message: 'Weird server error' });

            client.listRecordHeadersv0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                emptyLogger
            )
                .then((result) => new Error(result))
                .catch((error) => {
                    expect(error.isBoom).to.be.true();
                    expect(error.output.statusCode).to.equal(500);
                    expect(error.output.payload.error).to.equal('Internal Server Error');
                    expect(error.output.payload.message).to.equal('An internal server error occurred');
                    done();
                })
                .catch(done);
        });
    });

    describe('getStudyV0', () => {
        it('should return study for valid repo and identifier', (done) => {
            const study = {
                identifier: 'NSD1234',
                recordType: 'Study',
                prefLabel: { en: 'Some label' },
                title: { en: 'Some title' },
                subtitle: { en: 'Some sub title' },
                alternative: { en: 'Some alternative string' },
                abstract: { en: 'Some abstract' },
                analysisUnit: { en: 'Person' },
                ddiFile: ['DDIFile1'],
                fundedBy: [{ en: 'organization1' }],
                inGroup: ['studyGroupId1'],
                instrument: ['Instrument1'],
                kindOfData: { en: 'Kind of data' },
                subject: [{ en: 'Subject 1' }],
                product: 'LogicalDataset', // ID of logical dataset file
                purpose: { en: 'Some purpose' },
                universe: { en: 'Universe description for study' },
                variable: ['varid1'],
                available: '1970-01-01T01:00:00.000Z',
                temporal: {
                    startDate: '1970-01-01T01:00:00.000Z',
                    endDate: '1971-01-01T01:00:00.000Z',
                },
                spatial: [{ en: 'Norway' }],
            };

            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/Study/NSD1234')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, study, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getStudyV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                'NSD1234',
                emptyLogger)
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(result.payload).to.deep.equal(study);
                    done();
                })
                .catch(done);
        });
    });

    describe('getVariableV0', () => {
        it('should return variable for valid repo and identifier', (done) => {
            const variable = {
                identifier: 'Var1',
                recordType: 'Variable',
                prefLabel: { en: 'Some label' },
                description: { en: 'Some title' },
                notation: 'The variable name',
                basedOn: 'Id of represented variable?',
                analysisUnit: { en: 'Person' },
                concept: { en: 'Variable concept' },
                question: ['questionid1'],
                representation: [
                    {
                        prefLabel: {
                            en: 'Male',
                            no: 'Mann',
                        },
                        notation: '1',
                    },
                ],
                universe: { en: 'Same description as study?' },
            }; // End of variable

            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/Variable/Var1')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, variable, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getVariableV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                'Var1',
                emptyLogger)
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(result.payload).to.deep.equal(variable);
                    done();
                })
                .catch(done);
        });
    });

    describe('getQuestionV0', () => {
        it('should return question for valid repo and identifier', (done) => {
            const question = {
                identifier: 'Question1',
                recordType: 'Question',
                questionText: { en: 'My question' },
                responseDomain: ['domain1', 'domain2'],
                representation: [
                    {
                        prefLabel: {
                            en: 'Male',
                            no: 'Mann',
                        },
                        notation: '1',
                    },
                ],
            };

            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/Question/Question1')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, question, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getQuestionV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                'Question1',
                emptyLogger)
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(result.payload).to.deep.equal(question);
                    done();
                })
                .catch(done);
        });

        it('should fail when missing Last-Modified in headers', (done) => {
            const question = {
                identifier: 'Question1',
                recordType: 'Question',
                questionText: { en: 'My question' },
                responseDomain: ['domain1', 'domain2'],
                representation: [
                    {
                        prefLabel: {
                            en: 'Male',
                            no: 'Mann',
                        },
                        notation: '1',
                    },
                ],
            };

            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/Question/Question1')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, question);

            client.getQuestionV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                'Question1',
                emptyLogger)
                .then((result) => done(new Error(result)))
                .catch((error) => {
                    expect(error.isBoom).to.be.true();
                    expect(error.output.statusCode).to.equal(500);
                    done();
                })
                .catch(done);
        });
    });

    describe('getStudyGroupV0', () => {
        it('should return study group for valid repo and identifier', (done) => {
            // TODO :: ?
            // so this object is no longer valid, but because validation is done as the reply is leaving
            // the OSMH server, this (now) invalid studyGroup will not be detected as invalid.
            const studyGroup = {
                identifier: 'StudyGroup1',
                recordType: 'StudyGroup',
                prefLabel: { en: 'Some label' },
                title: { en: 'Study group title' },
                subtitle: { en: 'Study group sub title' },
                abstract: { en: 'abstract' },
                alternative: { en: 'alternative' },
                available: '1970-01-01T01:00:00.000Z',
                purpose: { en: 'Some purpose' },
                fundedBy: [
                    { en: 'organization1' },
                    { en: 'organization2' },
                ],
                inGroup: ['groupId'],
                kindOfData: { en: 'Kind of data' },
                universe: { en: 'Universe description for study' },
                ddiFile: ['DDIFile1'],
            };

            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/StudyGroup/StudyGroup1')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, studyGroup, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getStudyGroupV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {},
                'StudyGroup1',
                emptyLogger)
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(result.payload).to.deep.equal(studyGroup);
                    done();
                })
                .catch(done);
        });
    });

    describe('http header forwarding', () => {
        it('forwards any cache headers', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/StudyGroup/StudyGroup1')
                .matchHeader('If-Modified-Since', 'Mon, 03 Jan 2011 17:45:57 GMT')
                .matchHeader('If-None-Match', '686897696a7c876b7e')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, { message: 'Ok!' }, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getStudyGroupV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {
                    'If-Modified-Since': 'Mon, 03 Jan 2011 17:45:57 GMT',
                    'If-None-Match': '686897696a7c876b7e',
                },
                'StudyGroup1',
                emptyLogger)
                .then((result) => {
                    expect(result.payload).to.deep.equal({ message: 'Ok!' });
                    done();
                })
                .catch(done);
        });

        it('forwards x-request-id header', (done) => {
            nock(repositoryHandlerUrl)
                .get('/v0/GetRecord/StudyGroup/StudyGroup1')
                .matchHeader('X-Request-ID', 'totally-unique')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, { message: 'Ok!' }, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getStudyGroupV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {
                    'x-request-id': 'totally-unique',
                },
                'StudyGroup1',
                emptyLogger)
                .then((result) => {
                    expect(result.payload).to.deep.equal({ message: 'Ok!' });
                    done();
                })
                .catch(done);
        });

        it('does not forward unspecified headers', (done) => {
            nock(repositoryHandlerUrl, { badheaders: ['not-allowed'] })
                .get('/v0/GetRecord/StudyGroup/StudyGroup1')
                .query({ Repository: 'http://repository.example.com' })
                .reply(200, { message: 'Ok!' }, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

            client.getStudyGroupV0(
                'http://repositoryhandler.example.com',
                'http://repository.example.com',
                {
                    'x-request-id': 'so-unique',
                    'not-allowed': 'should cause request to fail',
                    'if-modified-since': 'Mon, 03 Jan 2011 17:45:57 GMT',
                },
                'StudyGroup1',
                emptyLogger)
                .then((result) => {
                    expect(result.payload).to.deep.equal({ message: 'Ok!' });
                    done();
                })
                .catch(done);
        });
    });
});
