const Code = require('code');
const Lab = require('lab');
const cessdaMetadataHarvester = require('../../index');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;

// We use server without starting it to provide a realistic testing ground
// that respects the server configuration. We need to nest the tests inside
// the callback.
cessdaMetadataHarvester((err, server) => {
    const resolutionServer = server.select('resolution-service');

    describe('resolution service server', () => {
        it('should be a valid hapi server', (done) => {
            expect(err).to.not.exist();
            expect(resolutionServer).to.exist();
            expect(resolutionServer.info.host).to.equal('localhost');
            expect(resolutionServer.info.port).to.equal(8081);
            done();
        });
    });

    describe('/v0/ResolveRepositoryURI', () => {
        it('should return repository handler uri when called with correct repo uri', (done) => {
            const repositoryURI = 'http://repository.example.com';
            const path = '/v0/ResolveRepositoryURL';
            const result = { repositoryHandlerUrl: 'http://repositoryhandler.example.com' };
            resolutionServer.inject(`${path}?Repository=${repositoryURI}`, (res) => {
                expect(res.statusCode).to.equal(200);
                expect(res.result).to.deep.equal(result);
                done();
            });
        });
    });
});
