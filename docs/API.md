# Detailed OSMH API documentation

After running the Node application the api documentation should be available at
[localhost:8080](http://localhost:8080). The server uses Swagger to render the
API specification and provides functionality to make requests against the API.

[![Swagger API Docs](./apidocs.png)](http://localhost:8080)

You can click an endpoint, say `/v0/GetRecord/Variable/{Identifier}`, to see
detailed information about the expected input and ouput from the endpoint.
Each endpoint will list a `Response Class (Status 200)` which is what you
should expect receiving if your request is successful. Additionally it will
show you response classes for 304, 400, and 500 responses. For each class
an example is available in the Swagger UI.

[![Response Classes](./responseclasses.png)](http://localhost:8080)

Each endpoint also lists its expected parameters (query, path, or body).
Swagger will provide the parameter model as well as an example. The example can
be copied into the input field for body type parameters.

[![Parameters](./parameters.png)](http://localhost:8080)

Use the `Try it out!` button to run a request. Swagger will run the request
against the harvester and show you the response.

## General properties about the API

### Headers

The OSMH API makes use of the `X-Request-ID`. Consumers should provide this
header when making requests against the harvester. If no `X-Request-ID` is
provided, the header will be added server-side and then passed along with the
response sent back to the consumer. The `X-Request-ID` can be used to follow
a single request through logs from different services.

The OSMH API makes use of caching headers and supports both `ETag` and
`Last-Modified` for any request going to a Repository Handler. Because the OSMH
does not directly deal with the repository data, it only forwards caching
headers present in consumer requests and repository handler responses.
You may pass `If-Modified-Since` and `If-None-Match` with the consumer requests
, but might not get back any caching headers in the response if the Repository
Handler in question does not support them.


### Parameters

All `Repository` specific requests require you to pass a `Repository` query
parameter with your request. The Harvester will send a request to the
Resolution Service with the URL passed in, and if the URL exists, receive a
Repository Handler URL. This is then used to make a request on the Repository
Handler server. If running the OSMH yourself please configure the mappings file
for the Resolution Service. See the `README.md file` for more info.


### Versioning

The API is versioned through the URL paths. Currently the version is `v0`. The
versions follows the major version of the OSMH project's semver package.json
version and is only updated for breaking changes.

### Properties

All record types includes an `identifier` property:

Study:

```json
{
  "identifier": "NSD0001",
  "title": "My study title"
}
```

The identifier property should uniquely identify that record for a given repository.
So for example `NSD0001` needs to be unique in the `http://repositoryA.nsd.no`
repository. The identifier property is used in the `/GetRecord/{RecordType}/{Identifier}`
endpoints. If a repository's record identifier contains URI components like `/` and `&`
the harvester will not be able to properly parse the
`GET http://harvester.example.com/v0/GetRecord/Study/myid/withslash` and the consumer will
receive a 404 not found. To avoid this the repository handlers should either use an
identifier without reserved URI components, or encode and decode these components in
requests and responses.

The `identifier` property is to be used as a OSMH specific identifier, and should not be
exposed to users in a consumer portal (e.g. search portal). The identifiers are not uuids,
or dois, and can as such not alone be used to uniquely identify a study across different
repositories. The only requirement for the `identifier` is that it in combination with a
`Repository` URL and a repository handler implementation identifies a record.

## /SupportedVersions

The only unversioned endpoint. It returns a JSON array of supported versions.
For example `["v0"]`.

## /ValidateRecord/{RecordType}

There are four `/v0/ValidateRecord/{RecordType}` endpoints, one for each of the
record types (e.g. study group, study, variable, and question). These can be
used to validate if a given JSON object conforms to the schemes defined by the
OSMH. Swagger will generate parameter classes based on the Harvester schemes,
and show them in the API docs. The endpoints can be useful when developing a
repository handler to check that the json you return from your handle will be
accepted by the harvester.

To validate a question record json document paste your json document into the
body value in Swagger and run it with the `Try it out!` button. You can then
also use the curl command Swagger generates for the request you performed for
other json documents to script the process.

[![Parameters](./questionvalidateresult.png)](http://localhost:8080)

## /ListSupportedRecordTypes

This endpoint returns a JSON array of supported record types, namely:
* StudyGroup
* Study
* Variable
* Question

Example:

```json
["StudyGroup", "Study", "Variable"]
```

## /ListRecordHeaders and /ListRecordHeaders{RecordType}

This endpoint list record headers for all record types or a single record type.
Each record header JSON object includes the identifier, record type, and
LastModified HTTP Date of the given record.

## /GetRecord/{RecordType}

These endpoints include `/GetRecord/StudyGroup`, `/GetRecord/Study`,
`/GetRecord/Variable`, and `/GetRecord/Question`. They are used to fetch a
record for a given `Repository`. Note that each endpoint requires the query
parameter `Repository` to be present, and the Repository URL to be present in
the mappings file in the Resolution Handler service.

Each `/GetRecord/{RecordType}` supports both `ETag` and `Last-Modified` style
caching headers. The Harvester will forward any caching headers (e.g.
`If-Modified-Since` and `If-None-Match`) from the consumer to the Repository
Handler, and will forward any `ETag` and `Last-Modified` from the Repository
Handler to the consumer.
